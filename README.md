# Video Seeker

### A PlanetCast webhook that fetches the latest videos.

A simple project that fetches the latest PlanetCast videos using YouTube Data API v3 and Google API Keys.

